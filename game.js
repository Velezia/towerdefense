window.addEventListener('load', canvasApp, false);
        
function canvasApp() {
    if (!document.createElement('canvas').getContext) {
        return;
    }
    
    var theCanvas = document.getElementById('canvas');
    var context = theCanvas.getContext('2d');  
    var startButton = document.getElementById('startButton');  
    var towerButton = document.getElementById('towerButton');
    var towerUpgradeButton = document.getElementById('towerUpgrade'); 
    var musicOffButton = document.getElementById('Music_off');
    var soundsOffButton = document.getElementById('Sounds_off');
    var lifeCounterView = document.getElementById('lifecounter');
    var goldCounterView = document.getElementById('goldcounter');
    var highscoreView = document.getElementById('highscore');
    var scoreView = document.getElementById('score');  
    var info = document.getElementById('info');
    
    //------------ONLY-FOR-PRÄSENTATION-------------
    /*
    Helfer für die Präsentation. Wird im regulären Spiel ausgeblendet.
    Gold und Leben sind beeinflussbar. Außerdem kann sofort das nächste
    Level gestartet werden oder das Spiel beendet.
    */
    var only_for_praesentation = document.getElementById('only_for_praesentation');
    var ofp_lifeButton = document.getElementById('lifeButton');
    var ofp_nextLvlButton = document.getElementById('nextLvlButton');
    var ofp_gameOver = document.getElementById('gameOverButton');
    var ofp_goldButton = document.getElementById('goldButton');
    ofp_lifeButton.addEventListener('click', function(){life -= 20; if(life <=0)life=1;}, false);
    ofp_nextLvlButton.addEventListener('click', levelEnd, false);
    ofp_gameOver.addEventListener('click', end, false);
    ofp_goldButton.addEventListener('click', function(){gold += 20; if(gold >=1000)gold=1000;}, false);
        
    //--------------------SOUND---------------------
    var music_bg = document.getElementById('backgroundmusic');
    var music_lvl = document.getElementById('levelmusic');
    var sound_shot = document.getElementById('shot');
    var sound_walk = document.getElementById('walk');
    var sound_dead = document.getElementById('dead');
    var sound_up = document.getElementById('up');
    var sound_damage = document.getElementById('damage');
    
    var music_volume = 10 / 100;
    var sound_volume = 3 / 100;
    var music_on = false;
    var sound_on = true;
    
    music_bg.volume = music_volume;
    music_lvl. volume = music_volume;
    sound_shot.volume = sound_volume;
    sound_walk.volume = sound_volume;
    sound_dead.volume = sound_volume;
    sound_up.volume = sound_volume;
    sound_damage.volume = sound_volume;
    
    var bgmusic_slider = document.getElementById("bgmusicVolumeRange");
    bgmusic_slider.addEventListener("input", setVolMusic);
        
    var sounds_slider = document.getElementById("soundsVolumeRange");
    sounds_slider.addEventListener("input", setVolSounds);
    
    //-----------------PIXELGRAFIKEN----------------    
    //Jellygrafiken
    var jelly1_walk = new Image();  //Jelly gelb laeuft
    var jelly1_dead = new Image();  //Jelly gelb verschwindet
    jelly1_walk.addEventListener('load', onImageLoaded);
    jelly1_dead.addEventListener('load', onImageLoaded);
    jelly1_walk.src = 'images/Jellys/yellow_walk.png';
    jelly1_dead.src = 'images/Jellys/yellow_dead.png';
    var jelly2_walk = new Image();  //Jelly pink laeuft
    var jelly2_dead = new Image();  //Jelly pink verschwindet
    jelly2_walk.addEventListener('load', onImageLoaded);
    jelly2_dead.addEventListener('load', onImageLoaded);
    jelly2_walk.src = 'images/Jellys/pink_walk.png';
    jelly2_dead.src = 'images/Jellys/pink_dead.png';
    var jelly3_walk = new Image();  //Jelly lila laeuft
    var jelly3_dead = new Image();  //Jelly lila verschwindet
    jelly3_walk.addEventListener('load', onImageLoaded);
    jelly3_dead.addEventListener('load', onImageLoaded);
    jelly3_walk.src = 'images/Jellys/purple_walk.png';
    jelly3_dead.src = 'images/Jellys/purple_dead.png';
    
    //Towergrafik
    var towerImage = new Image();
    towerImage.addEventListener('load', onImageLoaded);
    towerImage.src = 'images/Tower.png';
    
    //Hintergrundbilder
    var bg_Start = new Image();
    var bg_Lv1 = new Image();
    var bg_Lv2 = new Image();
    var bg_Lv3 = new Image();
    var bg_Lv4 = new Image();
    var bg_End = new Image();
    bg_Start.addEventListener('load', onImageLoaded);
    bg_Lv1.addEventListener('load', onImageLoaded);
    bg_Lv2.addEventListener('load', onImageLoaded);
    bg_Lv3.addEventListener('load', onImageLoaded);
    bg_Lv4.addEventListener('load', onImageLoaded);
    bg_End.addEventListener('load', onImageLoaded);
    bg_Start.src = 'images/Background_Start.png';
    bg_Lv1.src = 'images/Background_Lv1.png';
    bg_Lv2.src = 'images/Background_Lv2.png';
    bg_Lv3.src = 'images/Background_Lv3.png';
    bg_Lv4.src = 'images/Background_Lv4.png';
    bg_End.src = 'images/Background_End.png';
    
    var numImages = 13;     //Wie viele Bilder müssen geladen werden
    var imgLoadCount = 0;   //Counter wieviele Bilder schon geladen wurden
    
    //SpriteSheet Angaben
    var spriteSize = 128;           //Größe einer Grafik
    var spriteSheetColumms = 4;     //wieviele Spalten gibt es auf dem Bild
    var spriteSheetRows = 4;        //wieviele Zeilen gibt es auf dem Bild
    var numSpriteFrames = spriteSheetColumms * spriteSheetRows; //wieviele Frames gibt es für eine Animation
    var spriteFrameID = 0;          //aktuelle Grafik eines SpriteSheets welche aufgerufen wird
    
    //-----------------EVENTS-------------------    
    theCanvas.addEventListener('mousedown', mouseclick, false);
    theCanvas.addEventListener('mousemove', mousemove, false);
    document.addEventListener('keydown',tastaturEingabe, false);
    startButton.addEventListener('click', start, false);
    towerButton.addEventListener('click', function(){toDoOnCanvas = "setTower";}, false);
    towerUpgradeButton.addEventListener('click',function(){toDoOnCanvas = "towerUp";}, false);
    musicOffButton.addEventListener('click', musicOff, false);
    soundsOffButton.addEventListener('click', soundOff, false);
    
    //-----------ALLGEMEINE-VARIABLEN-----------    
    var toDoOnCanvas;       //was möchtest du als nächstes auf dem Canvas tun
    var mousePosX = 0;      //Mousposition auf der Seite X
    var mousePosY = 0;      //Mousposition auf der Seite Y 
    var frames = 0;         //Zaehler     
    //-----------------USER-STATS----------------    
    var life = 100;             //aktuelle Lebenspunkte
    var gold = 0;               //aktuelles Gold
    var score = 0;              //aktuelle Punkte
    var highscore = 0;          //zuletzt erreichte Highscore
    //-----------------LEVEL----------------
    var start = false;          //läuft das Spiel
    var newstart = false;       //soll ein neues Spiel gestartet werden
    
    var currentLvl = 0
    var level = new Array;  
        level[0] = new Level(
            "Level 1",          //Name
            6,                  //Anzahl Gegner Level1 6
            12,                 //Anzahl Gegner Level2 12
            5,                  //Anzahl Gegner Level3 5
            15,                 //Startgold
            [{x: 0, y: 150},    //Pfad
            {x: 700, y: 150},
            {x: 700, y: 300},
            {x: 100, y: 300},
            {x: 100, y: 450},
            {x: 800, y: 450}]                 

        );
        level[1] = new Level(
            "Level 2",          //Name
            5,                  //Anzahl Gegner Level1 5
            10,                 //Anzahl Gegner Level2 10
            5,                  //Anzahl Gegner Level3 5
            10,                 //Startgold
            [{x: 0, y: 300},    //Pfad
            {x: 300, y: 300},
            {x: 300, y: 100},
            {x: 500, y: 100},
            {x: 500, y: 500},
            {x: 700, y: 500},
            {x: 700, y: 300},
            {x: 800, y:300}] 
        );    
        level[2] = new Level(
            "Level 3",          //Name
            2,                  //Anzahl Gegner Level1 2
            7,                  //Anzahl Gegner Level2 7
            12,                 //Anzahl Gegner Level3 12
            10,                 //Startgold
            [{x: 0, y: 300},    //Pfad
            {x: 300, y: 300},
            {x: 300, y: 500},
            {x: 500, y: 500},
            {x: 500, y: 100},
            {x: 700, y: 100},
            {x: 700, y: 300},
            {x: 800, y:300}]  
        );    
        level[3] = new Level(
            "Level 4",          //Name
            0,                  //Anzahl Gegner Level1 0
            14,                 //Anzahl Gegner Level2 14
            8,                  //Anzahl Gegner Level3 8
            10,                 //Startgold
            [{x: 0, y: 150},    //Pfad
            {x: 600, y: 150},
            {x: 600, y: 450},
            {x: 100, y: 450},
            {x: 100, y: 300},
            {x: 800, y: 300}]  
        );
    
    var enemy_counter = 0;          //aktuelle Anzahl der Gegner
    var enemy_num = 0;              //Zaehler
    var enemy_army = new Array;     //alle Gegner
    var enemy_army_run = new Array; //Gegner werden auf dem Canvas gezeichnet
    
    var allLastAni;                 //wurden alle letzten Animationen ausgeführt
    var lastAniCount = 0;           //die letzten Animationen werden mitgezählt
    var infoActivate = false;
    
    var tower = new Array;
    var tower_num = 0;              //aktuelle Anzahl Tower
    var tempTower_color = 'orange'; //aussehen
    var towerOnPath = false;        //befindet sich die Maus auf dem Pfad wenn ein Tower platziert werden soll
    var upgradeOnTower = false;     //soll ein Tower geupgradet werden   
    
    //-----------------INITIAL-GUI----------------    
    towerButton.style.visibility = 'hidden';
    towerUpgradeButton.style.visibility = 'hidden'; 
    lifeCounterView.style.visibility = 'hidden';
    goldCounterView.style.visibility = 'hidden';
    scoreView.style.visibility = 'hidden';
    highscoreView.style.visibility = 'hidden';
    info.style.visibility = 'hidden';
    
    musicOffButton.style.backgroundColor = '#476e9e';
    musicOffButton.style.color = '#b0afaf';
    musicOffButton.innerHTML = "on";
    bgmusic_slider.value = 0;
    
    //nur zur Präsentationszwecken einblenden
    //only_for_praesentation.style.visibility = 'hidden';
    
    //-----------------LOOP-----------------
    
    //Starte Rendering Loop erst ausführen wenn alle Grafiken geladen sind
    function onImageLoaded(e) {            
            
        imgLoadCount++;
        if (imgLoadCount == numImages)
        {
            theCanvas.style.cursor = 'default';
            detectIE();
            renderingLoop();        
        }  
        else{            
            theCanvas.style.cursor = 'wait'; 
        }
    }
    
    //Rendering Loop - für drawCanvas jeden Frame aus
    function renderingLoop(){
        requestAnimationFrame(renderingLoop);
        drawCanvas();
    }        
    
    //Zeichnet alle Canvas Objekte 60x pro Sekunde neu    
    function drawCanvas() {
        
        frames++;       //Anzahl der Frames die schon erstellt wurden
        
        var infoViewtime = 150;
        var teilbar = frames % infoViewtime;
                
        /*Es muss erst ein Rechteck erstellt werden, damit der Bereich gecleart werden kann
        theCanvas.width = theCanvas.width; Erstellt vermutlich ebenfalls ein Rechteck.*/
        theCanvas.width = theCanvas.width;
        //context.fillRect(0,0, context.width, context.height);
        //Canvas leeren
        context.clearRect(0,0, context.width, context.height);
        
        if (level[currentLvl].isRunning == true && start == true){
            
            //aktualisiert die Counter und die Button
            updateGUI();    
            
            //zeichnet den vordefinierter Weg 
            //path(level[currentLvl].waypoint); //<<<<<< wird ausgeblendet
            
            //Zeichnet die Gegner    
            enemy_draw(); 
            
            if(toDoOnCanvas == 'towerUp'){
               for(var i = 0; i<tower.length; i++){                    
                    
                    if(tower[i].level == 1){

                        tower[i].drawReach(tower[i].previewReach);
                    }   
                } 
            }
            
            //zeichnet die Tower
            tower_draw();  
            
            //zeichnet die Previewansichten vom Tower
            previewTower();
            
            //sind keine Gegner mehr da und wurden die letzten Animationen alle abgespielt - beende das Level
            if (enemy_counter <= 0 && lastAniCount == enemy_army.length){
                
                levelEnd();
            }
        } 
        else{
            if(start == true){
                //zeichnet die Tower
                tower_draw(); 
            }
        }        
        
        //Infoeinblendung
        if(teilbar == 1 && info.style.visibility == 'visible'){
           info.style.visibility = 'hidden';
        }
    }  
    
    //--------ALLGEMEINE-METHODEN------------

    //Startet das Spiel
    function start(){
        
        if (newstart == true){
            end();
        }
        else{            
            //Musik on/off
            if(music_on == true){
                music_bg.pause();
                music_lvl.play();
            }
            
            start = true;
            
            //Beginn das nächste Level
            level[currentLvl].next();

            //Startbutton ausblenden
            startButton.style.visibility = 'hidden';            

            //GUI einbelnden
            towerButton.style.visibility = 'visible';
            towerUpgradeButton.style.visibility = 'visible'; 
            lifeCounterView.style.visibility = 'visible';
            goldCounterView.style.visibility = 'visible';
            scoreView.style.visibility = 'visible'; 
            highscoreView.style.visibility = 'visible';
            activateButton(towerUpgradeButton, false);
            
            console.log(level[currentLvl].name + " gestartet."); 
            infoActivate = true;
            infoView(level[currentLvl].name);            
        }
    }   
    
    //beendet das aktuelle Level
    function levelEnd(){
        level[currentLvl].isRunning = false;
        startButton.style.visibility = 'visible';
        startButton.innerHTML = "next Level";
        
        //wurde kein Tower gesetzt gibt es keine Punkte
        if(tower.length > 0){            
            //Ist das Level beendet gibt es für jeden Lebenspunkt noch einen Punkt extra
            //und für jedes übrige Gold gibt es 2 Punkte extra
            score += (gold * 2);
            score += life;
            console.log("+" + (gold*2) + " Points (Gold²)");
            console.log("+" + life + " Points (Life)");  
            infoActivate = true;
            infoView(((gold * 2) + life) + " points");
        }     
        
        lifeCounterView.innerHTML = "Life: " + life;
        goldCounterView.innerHTML = "Gold: " + gold;
        scoreView.innerHTML = "Score: " + score;
        
        //GUI ausblenden
        towerButton.style.visibility = 'hidden';
        towerUpgradeButton.style.visibility = 'hidden';
        
        console.log(level[currentLvl].name + " beendet.");
        
        if(currentLvl == level.length-1){ 
                           
            startButton.innerHTML = "Game Over";
            newstart = true;       
            
            //Highscore setzen falls der Score größer als der letzte Highscore ist
            //und der Gegner das Spiel nicht verloren hat
            if(score > highscore && life > 0){
            
                highscore = score;
                highscoreView.innerHTML = "Highscore: " + highscore;
            } 
        }
        else{
           currentLvl++;
        }        
        
        highscoreView.innerHTML = "Highscore: " + highscore;
    }
    
    //beendet das Spiel
    function end(){
        
        //Musik on/off
        music_lvl.pause();        
        if(music_on == true){            
            music_bg.play();
        }
        
        start = false;
        startButton.style.visibility = 'visible';
        startButton.innerHTML = "play again";
        //Bild wird noch angepasst
        theCanvas.style.backgroundImage = 'url(images/Background_End.png)';
        
        //Startstats
        currentLvl = 0;
        gold = 0;
        life = 100;
        score = 0;
        
        //GUI ausblenden
        towerButton.style.visibility = 'hidden';
        towerUpgradeButton.style.visibility = 'hidden'; 
        lifeCounterView.style.visibility = 'hidden';
        goldCounterView.style.visibility = 'hidden';
        scoreView.style.visibility = 'hidden';
        
        newstart = false;
        console.log("Spiel beendet.");
    }
       
    //aktualisiert die Counter und die Button
    function updateGUI(){
                    
        //Counter aktualisieren          
        lifeCounterView.innerHTML = "Life: " + life;
        goldCounterView.innerHTML = "Gold: " + gold;
        scoreView.innerHTML = "Score: " + score;
        highscoreView.innerHTML = "Highscore: " + highscore;
            
        //Towerbutton aktiveren/deaktivieren anhand des Goldes
        //ein Tower und ein Upgrade kostet 5 Gold
        if (gold < 5){                
            //Button deaktivieren
            activateButton(towerButton, false);
            activateButton(towerUpgradeButton, false);
        } 
        else{
            //Tower-Button aktivieren
            activateButton(towerButton, true);
            
            /*Wenn kein Tower auf dem Spielfeld ist kann keiner geupgradet werden
            und der Button dafür wird deaktiviert.
            Wenn alle Türme schon geupgradet wurden, wird der Button
            ebenfalls ausgeblendet.*/
            if(tower.length > 0){
                
                for(var i = 0; i < tower.length; i++){
                    
                    if(tower[i].level < 2){
                        activateButton(towerUpgradeButton, true)                          
                    }
                }
            } 
        }
        
        if(toDoOnCanvas == 'setTower'){
            //deaktiviert den Button
            activateButton(towerButton, false);
        }
    }
    
    /*
    Durch die toDoOnCanvas Variable kann bestimmt werden, welche Aktion beim nächsten Mausclick
    auf dem Canvas ausgefuehrt werden soll.
    Türme werden gesetzt, Türme werden geupgradet.
    */
    function mouseclick(event){
        
        this.event = event;
        
        //wurde der Tower-Button aktiviert wird mit dem nächsten Mausclick ein Tower platziert
        if (toDoOnCanvas == 'setTower' && towerOnPath == false){
            tower_create();
            toDoOnCanvas = null;
            sound_up.play();
            //blendet den Cursor wieder sein
            theCanvas.style.cursor = 'default';            
            //aktiviert den Tower Button
            activateButton(towerButton, true);
        }
        
        //wurde der Upgrade-Button aktiviert wird mit dem nächsten Mausclick auf einen Tower dieser geupgradet
        if (toDoOnCanvas == 'towerUp' && upgradeOnTower == true)
        {
            //setzt den Cursor wieder zurück
            theCanvas.style.cursor = 'default';
            toDoOnCanvas = null;
            sound_up.play();
            
            //setzt das aussehen der Tower wieder auf default
            for(var i = 0; i<tower.length; i++){
                
                //bisauf die Tower die upgegradet wurden
                if(tower[i].up == true){
                    
                    tower[i].color = 'blue';
                    tower[i].level = 2;
                    tower[i].power = 2;
                    tower[i].shotLineWidth = 5; //default ist 3
                    tower[i].reach = 100;
                }
                else{
                    if(tower[i].level == 1)
                    {
                       tower[i].color = 'orange'; 
                    }
                }
            }
            
            score += 10;            
            gold -= 5; //ein Upgrade kostet 5 Gold
            //Info einblenden
            infoActivate = true;
            infoView("+10 points");
        }

    }
   
    /*
    Ueberprueft permanent die Position der Maus auf dem Canvas.
    Erstellt Previewansichten für die Tower.
    */
    function mousemove(event){
        
        //ermittelt die genaue Position auf dem Canvas
        mousePosX = event.clientX - theCanvas.offsetLeft;
        mousePosY = event.clientY - theCanvas.offsetTop;
        
        //Bestimmt was auf dem Canvas passiert anhand des Buttons der aktiviert wurde.  
        switch (toDoOnCanvas){
            
            /*
            Wird auf den Tower-Button gedrückt, wird direkt danach überprüft, ob sich die Maus
            auf dem Pfad befindet. Ist das nicht der Fall bleibt toweronPath auf false und mit dem
            nächsten Klick wird ein Tower auf diese Stelle platziert. Siehe mouseclick().
            */
            case 'setTower':
                
                //blendet den Cursor aus
                theCanvas.style.cursor = 'none';            

                //Tower kann nicht auf den Pfad gesetzt werden
                context.beginPath();
                context.lineWidth = 80;
                context.moveTo(level[currentLvl].waypoint[0].x, level[currentLvl].waypoint[0].y);

                for(var i=1; i < level[currentLvl].waypoint.length; i++)                {    
                    context.lineTo(level[currentLvl].waypoint[i].x, level[currentLvl].waypoint[i].y)
                };

                if(context.isPointInStroke(mousePosX,mousePosY)){                
                    tempTower_color = "grey";
                    towerOnPath = true;
                    return;
                }
                else{
                    tempTower_color = "orange";
                    towerOnPath = false;
                }
                context.closePath();

                //Tower kann nicht auf andere Tower gesetzt werden
                for(var i = 0; i< tower.length; i++){
                    context.beginPath();
                    context.moveTo(tower[i].x, tower[i].y);
                    context.rect(tower[i].x-85, tower[i].y-150, 175,180);
                    //context.arc(tower[i].x, tower[i].y-35, 70, 0, 2*Math.PI);

                    if(context.isPointInPath(mousePosX,mousePosY)){                
                        tempTower_color = "grey";
                        towerOnPath = true;
                        return;
                    }
                    else{
                        tempTower_color = "orange";
                        towerOnPath = false;
                    }
                context.closePath();   
                } 
                break;
            
            /*
            Wird der Upgrade-Button gedrückt, wird direkt danach überprüft ob sich die Maus,
            im Bereich eines Towers ist. Ist das der Fall wird upgradeOnTower auf true gesetzt
            und der nächste Mausklick platziert das Upgrade auf dem Tower. Siehe mouseclick()).
            */
            case 'towerUp':
                
                for(var i = 0; i< tower.length; i++){
                    context.beginPath();
                    context.moveTo(tower[i].x, tower[i].y);
                    //context.arc(tower[i].x, tower[i].y-35, 70, 0, 2*Math.PI);
                    context.rect(tower[i].x-35, tower[i].y-80, 70,100);


                    if(context.isPointInPath(mousePosX,mousePosY) && tower[i].level == 1){                
                        upgradeOnTower = true;
                        theCanvas.style.cursor = 'pointer';
                        tower[i].up = true;
                        tower[i].previewReach = 100;
                        return;
                    }
                    else{
                        theCanvas.style.cursor = 'crosshair';
                        upgradeOnTower = false;
                        tower[i].up = false;
                        tower[i].previewReach = tower[i].reach;
                    }
                    context.closePath();   
                }                 
                break;
                
            default:
                break;
        }
    }    
    
    //Satz des Pythagoras. (Berechnet den Abstand zwischen Tower und Enemy.)
    function calculateReach(a, b){        
        this.a = a;
        this.b = b;        
        //     Wurzel aus           a²   +           b²
        var c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2))        
        return c;        
    }    
    
    //Aktiviert und deaktiviert einen Button. 
    function activateButton(button, activate){
        
        this.button = button;
        this.activate = activate;
        
        //deaktiveren
        if(activate == false){
            button.setAttribute("disabled", true);
            button.style.backgroundColor = '#476e9e';
            button.style.color = '#b0afaf';
            button.style.cursor = "default"; 
        }
        else{
            button.removeAttribute("disabled", true);
            button.style.backgroundColor = '#7892c2';
            button.style.color = '#ffffff';
            button.style.cursor = "pointer";
        }
    }    
    
    //Wird ausgeführt wenn eine Taste auf der Tastatur gedrück wird.
    function tastaturEingabe(e){
        
        //console.log(e.keyCode);
        
        switch (e.keyCode){
                
            case 27: //ESC
                switch(toDoOnCanvas){
                    case "setTower":
                        toDoOnCanvas = null;
                        theCanvas.style.cursor = 'default';
                        activateButton(towerButton, true);
                        break;
                        
                    case "towerUp":
                        toDoOnCanvas = null;
                        theCanvas.style.cursor = 'default';
                        activateButton(towerUpgradeButton, true);
                        //setzt das aussehen der Tower wieder auf default
                        for(var i = 0; i<tower.length; i++){                
                            tower[i].color = 'orange';
                        }
                        break;

                    default:
                        break; 
                }
                break;
            
            //kann beliebig erweitert werden
            //case :
                
            default:
                break;
        } 
    }
    
    /*
    Eine Info kann unten rechts unterm Canvas eingeblendet werden. 
    Bleibt ca. 2 Sekunden und verschwindet dann wieder.
    */
    function infoView(text){
        this.text = text;
      
        if(infoActivate == true){            
            info.style.visibility = 'visible';
            info.innerHTML = text;
        }
        else{
            info.style.visibility = 'hidden';
        }
    }    
    
    //Mischt ein Array.    
    Array.prototype.shuffle = function() {
    
        var i = this.length;
        
        //ist im Array nur ein oder kein Element breche ab 
        if(i < 2){
            return;   
        }
        
        //bestimmt für ein Element eine neue zufälligposition und tauscht diese mit dem anderen Element aus
        do{
            var ri = Math.floor(Math.random() * i);
            var tempPos = this[ri];
            this[ri] = this[--i];
            this[i] = tempPos;
        }
        while(i)
 
    }
    
    //Überprüft ob sich ein Element im Array befindet 
    //(array.includes funktioniert im IE nicht)
    Array.prototype.include = function(value){
	   var returnValue = false;
	   var pos = this.indexOf(value);
	   if (pos >= 0) {
		  returnValue = true;
	   }
	   return returnValue;
    }
    
    //Bestimmt den Browser des Nutzers (nicht selbst geschrieben)
    function detectIE() { 
        if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) 
        {
            //alert('Opera');
        }
        else if(navigator.userAgent.indexOf("Chrome") != -1 )
        {
            //alert('Chrome');
        }
        else if(navigator.userAgent.indexOf("Safari") != -1)
        {
            //alert('Safari');
        }
        else if(navigator.userAgent.indexOf("Firefox") != -1 ) 
        {
             //alert('Firefox');
        }
        else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
        {
          alert('Jelly Attack kann im Internet Explorer fehlerhaft sein. Bitte benutzen Sie wenn möglich einen anderen Browser.'); 
        }  
        else 
        {
           //alert('unknown');
        }
    }
 
    //-----------------TOWER----------------    
    
    /*
    Towerklasse
    Position auf dem Canvas (durch Mausclick), Level, Farbe, Radius, Reichweite
    */
    function Tower(x,y,level,color,radius,reach, shot, name, costs, power){ 
        this.level = level; //Stufe
        this.color = color; //farbe
        this.radius = radius; //Groesse
        this.reach = reach; //Reichweite
        var previewReach = reach; //Reichweite in der Previewansicht;
        this.shot = shot;   //geschossen?
        this.name = name;   //Name
        this.enemyInReach = new Array;
        this.costs = costs //Kosten
        this.power = power //Stärke
        
        this.x = x;         //aktuelle x-Position auf dem Canvas
        this.y = y;         //aktuelle y-Position auf dem Canvas
        this.up = false;
        this.shotLineWidth = 3;     //Laserdicke
        this.shotColor = '#476e9e' //Laserfarbe
    }    
    
    //Tower:zeichnet den Tower auf dem Canvas    
    Tower.prototype.draw = function(){
                    
        //Tower
        /*context.beginPath();
        context.moveTo(this.x, this.y);
        context.fillStyle = this.color;
        context.arc(this.x, this.y, this.radius, 0, 2*Math.PI);
        context.fill();
        context.closePath();*/ 
        
        /*BAUM - SPRITESHEET:  col.0      col.1
                    row 0    default / distroyed
                    row 1   preview / upgrade        
        */
        var column = 0;
        var row = 0;
        switch (this.color){
                
            case 'grey':

                column = 0;
                row = 1;
                break;

            case 'orange':

                column = 0;
                row = 0;
                break;
                
            case 'blue':

                column = 1;
                row = 1;
                break;
                
            case 'aquamarine':
                
                column = 1;
                row = 0;
                break;

            default:
                break;
        }
        
        var sourceX = column * spriteSize*2;
        var sourceY = row * spriteSize*2;
        
        
        
        //Quelle, welcher Bereich auf dem Bild X&Y, wie groß auf dem Bild, wohin auf dem Canvas X&Y, wiegroß auf dem Canvas X&Y 
        context.drawImage(towerImage, sourceX, sourceY, spriteSize*2, spriteSize*2, this.x - spriteSize, this.y - spriteSize, spriteSize*2, spriteSize*2);
        
        
    }    
    
    //Tower: zeichnet die Reichweite vom Tower auf den Canvas.     
    Tower.prototype.drawReach = function(newReach){
        
        this.newReach = newReach;
        
        //sichtbare Reichweite des Towers
        context.beginPath();
        context.moveTo(this.x, this.y);
        context.fillStyle = 'rgba(71,110,158,0.3)';
        context.arc(this.x, this.y, newReach, 0, 2*Math.PI);
        context.fill(); 
        context.closePath();
    }
    
    /*
    Jeder Tower überprüt ob sich ein Gegner in Reichweite befindet. Ist das der Fall wird er als 
    für den Turm in Reichweite markiert. 
    Die Gegner die fuer den Turm in Reichweite sind werden in ein Array des Turms geschrieben und 
    der Turm, schießt immer auf das erste Element in seinem Array.
    Hat ein Gegner keine Lebenspunkte mehr oder ist nicht mehr in Reichweite, wird der Gegner
    aus dem Array entfernt. Wird in tower_draw() aufgerufen.
    */        
    Tower.prototype.shotOn = function(){
                
        var shottime = 45; //0,75 Sekunden
        var teilbar = frames % shottime;
                
        //wird alle 45 Frames aufgerufen 
        if(teilbar == 1){
                
            //prueft jeden Gegner auf dem Spielfeld
            for(var i = 0; i<enemy_army_run.length; i++){
                    
                a = enemy_army_run[i].x - this.x;
                b = enemy_army_run[i].y - this.y;   
                c = calculateReach(a,b) - enemy_army_run[i].radius;
                    
                //ist der Gegner in Reichweite
                if (c <= this.reach){
                        
                    //ist der Gegner noch nicht im Reichweiten Array und ist er nicht tot
                    if (!this.enemyInReach.include(enemy_army_run[i]) && !enemy_army_run[i].dead){
                        //fuege Gegner in Reichweiten Array hinzu
                        this.enemyInReach.push(enemy_army_run[i]);   
                            
                        //console.log(enemy_army_run[i].name + " ist in Reichweite von  " + this.name);
                    } 
                }    
             }
                
            //gibt es Gegner in Reichweite
            if(this.enemyInReach.length > 0){
                    
                //schiesse auf den ersten Gegner im Reichweiten Array
                this.drawShot();
                console.log(this.name + " schießt auf  " + this.enemyInReach[0].name);

                this.enemyInReach[0].life -= this.power;
                console.log(this.enemyInReach[0].name + " -" + this.power + " life" )

                //hat der Gegner keine Lebenpunkte mehr
                if(this.enemyInReach[0].life <= 0 && this.enemyInReach[0].dead == false){
                    //markieren ihn als tot
                    this.enemyInReach[0].dead = true;

                    gold += this.enemyInReach[0].loot;
                    score += (this.enemyInReach[0].lv * 2);

                    console.log("+" + this.enemyInReach[0].loot + " Gold");
                    console.log("+" + (this.enemyInReach[0].lv * 2) + " Points (EnemyLv²)");
                    infoActivate = true;
                    infoView("+"+ (this.enemyInReach[0].lv * 2) + " points"); 

                    enemy_counter--;

                    console.log(this.enemyInReach[0].name + " ist tot.");
                    console.log("noch " + enemy_counter + " Gegner");
                }
            }
                
                //laufe ueber alle Gegner im Reichweiten Array von hinten
            for(var i = this.enemyInReach.length-1; i >= 0; i--){
                    
                var aa =   this.enemyInReach[i].x - this.x;
                var bb =   this.enemyInReach[i].y - this.y; 
                var cc =   calculateReach(aa,bb) - this.enemyInReach[i].radius;
                    
                //ist ein Gegner tot oder nicht mehr in Reichweite
                if(this.enemyInReach[i].dead || cc > this.reach){
                        
                    //console.log(this.enemyInReach[i].name + " wird aus dem Array von " + this.name + " gelöscht");
                        
                    //entferne ihn aus dem Array (postion im array, anzahl)
                    this.enemyInReach.splice(i, 1);
                }
            }
        }
    }    
    
    //Tower: Schuss auf Gegner wird grafisch dargestellt    
    Tower.prototype.drawShot = function(){
        
        sound_shot.play();
        
        context.save();
        context.lineCap = 'round';
        context.strokeStyle = this.shotColor;
        context.lineWidth = this.shotLineWidth;
        
        
        context.beginPath();
        context.moveTo(this.x, this.y-10);
        context.lineTo(this.enemyInReach[0].x, this.enemyInReach[0].y);
        context.stroke();
        context.closePath();
        context.restore();  
        context.closePath();
    }
        
    /*
    Ein neuer Tower wird in das Tower Array geschrieben und auf dem Canvas
    platziert. Siehe mouseclick().
    */
    function tower_create(){
        
        tower[tower_num] = new Tower(
            mousePosX,  //x-Position auf dem Canvas
            mousePosY,  //x-Position auf dem Canvas
            1,          //Level
            'orange',   //Aussehen
            20,         //Groesse (Radius)
            80,         //Reichweite (Radius)
            false,      //geschossen?
            "Tower_" + tower_num,    //name
            5,          //Kosten
            1           //Stärke
        )
        
        score += tower[tower_num].costs;
        gold -= tower[tower_num].costs;
        console.log("-" + tower[tower_num].costs + " Gold");
        console.log("+" + tower[tower_num].costs + " Points");
        //Info einblenden
        infoActivate = true;
        infoView("+"+ tower[tower_num].costs + " Points");
        
        tower_num++;
    }
    
    /*
    Wird in der drawCanvas aufgerufen.
    Es werden alle Tower die im Tower Array vorhanden sind gezeichnet.
    Jeder Tower schießt auf jeden Gegner der in seiner Reichweite ist.    
    */
    function tower_draw(){
        
        for(var i = 0; i<tower.length; i++){
            
            tower[i].draw();
        }  
        
        if(level[currentLvl].isRunning == true){
            
            for(var i = 0; i<tower.length; i++){
                
                tower[i].shotOn();
            }
        }
    }    
    
    //Zeichnet Previewansichten vom Tower auf dem Canvas.    
    function previewTower(){
        //Previewansicht vom Tower
        if (toDoOnCanvas == 'setTower'){
            var temp_tower = new Tower(mousePosX,mousePosY,0,tempTower_color,20,80, false, "temp_tower", 0, 0);
            temp_tower.drawReach(temp_tower.reach);
            temp_tower.draw();                
        }
            
        //Previweansicht für TowerUpgrade
        if (toDoOnCanvas == 'towerUp'){
            //var temp_tower = new Tower(mousePosX,mousePosY,1,'blue',20,80, false, "temp_tower");
            //temp_tower.draw(temp_tower); 
                
            //Button deaktivieren
            activateButton(towerUpgradeButton, false);

            for(var i = 0; i<tower.length; i++){                    
                    
                if(tower[i].level == 1){
                        
                    tower[i].color = 'aquamarine';
                }   
            }   
        }    
    }
        
    //-----------------ENEMYS----------------    
    
    //Gegnerklasse   
    function Enemy(name,lv,life,color,speed,loot){ 
        this.name = name;   //Name       
        this.lv = lv;       //Stufe
        this.life = life;   //Lebenspunkte
        this.color = color; //farbe
        this.speed = speed; //Geschwindigkeit -> Schritte pro Frame
        this.loot = loot;   //dropt Gold wenn tot
        
        this.spriteTime = null;     //Zeitpunkt an dem das Spielfeld betreten wurde
        this.radius = 10;           //Groesse für Previewkreis - später ausgeblendet
        this.dead = false;          //hat der Gegner keine Lebenspunkte mehr
        this.spriteFrameID = 1;     //welcher Sprite wird gezeichnet
        this.spriteCount = false;   //wird benötigt um den nächsten Sprite zu bestimmen
        this.spriteCount2 = false;  //wird benötigt um den nächsten Sprite zu bestimmen
        
        
        this.x = level[currentLvl].waypoint[0].x;         //aktuelle x-Position auf dem Canvas
        this.y = level[currentLvl].waypoint[0].y;         //aktuelle y-Position auf dem Canvas
        this.i = 0;                                       //zaehler
        this.maxX = level[currentLvl].waypoint[this.i].x; //max laenger der Teilstrecke x
        this.maxY = level[currentLvl].waypoint[this.i].y; //max laenger der Teilstrecke y
        this.maxXfinish = false;                          //wurde die komplette x Teilstrecke abgelaufen
        this.maxYfinish = true;                           //wurde die komplette y Teilstrecke abgelaufen
        this.direction = 'goRight';                       //in welche Richtung wird gelaufen
        this.lastAni = false;                             //wurde die letzte Animation schon abgespielt
        this.lastAnibegin = false;                        //hat die letzte Animation begonnen
    }    
    
    //Enemy: Ein Gegner läuft den definierten Weg entlang    
    Enemy.prototype.animation = function (){        
                
        //sind die Wegpunkte von den Teilstrecken x und y abgelaufen
        if(this.x == this.maxX && this.y == this.maxY)
        {
            //sind alle Wegpunkte abgelaufen beende die Animation
            if (this.i == level[currentLvl].waypoint.length-1){
                
                /*
                Ist der Gegner bis zum Ende durch gekommen verliert
                der Spieler die Lebenpunkte die der Gegner noch übrig hat.
                */
                if(this.dead == false){
                    
                    sound_damage.play();
                    life -= this.life;
                    console.log("-" + this.life + " Life");
                    
                    //markiere den Gegner ebenfalls als tot
                    this.dead = true;
                    enemy_counter--;
                    this.lastAni = true;
                    lastAniCount++;
                    
                    //gelangen die Lebenspunkte unter 0 bleiben sie bei 0 und das Spiel ist zuende
                    if(life <=0){
                        
                        life = 0;
                        
                        currentLvl = level.length-1;
                        levelEnd();
                    }
                }
            }
            //uebergebe die naechsten Wegpunkte
            else{           
                this.i++;
                this.maxX = level[currentLvl].waypoint[this.i].x;
                this.maxY = level[currentLvl].waypoint[this.i].y;
            }
        }
        else
        {
            //ist die Teilstrecke x noch nicht fertig laufe weiter
            if (this.maxXfinish == false)
            {
                //laufe rueckwerts wenn die aktuelle x-Position groesser als maxX ist
                if (this.x >= this.maxX) 
                {
                    this.x-=this.speed;
                    this.direction = "goLeft";
                    
                    //ist x am Ende der Teilstrecke durch einen ungerade speed-groesse kleiner als maxX setze x auf maxX
                    //und beende die Teilstrecke
                    if (this.x <= this.maxX)
                    {
                        this.X = this.maxX;
                        this.maxYfinish = false;
                        this.maxXfinish = true;
                    }
                }
                else
                {   //laufe vorwaerts wenn die aktuelle x-Position kleiner als maxX ist
                    if (this.x <= this.maxX)
                    {   
                        this.x+=this.speed;
                        this.direction = "goRight";
                        
                        //ist x am Ende der Teilstrecke durch einen ungerade speed-groesse goesser als maxX setze x auf maxX
                        //und beende die Teilstrecke
                        if (this.x >= this.maxX)
                        {
                            this.x = this.maxX;
                            this.maxYfinish = false;
                            this.maxXfinish = true;
                        }
                    }
                }
            }
            
            //ist die Teilstrecke y noch nicht fertig laufe weiter
            if (this.maxYfinish == false)
            {
                //laufe nach oben wenn die aktuelle y-Position groesser als maxY ist
                if (this.y >= this.maxY) 
                {
                    this.y-=this.speed;
                    this.direction = "goUp";
                    
                    //ist y am Ende einer Teilstrecke durch einen ungerade speed-groesse kleiner als maxY setze y auf maxY
                    //und beende die Teilstrecke
                    if (this.y <= this.maxY)
                    {
                        this.y = this.maxY;
                        this.maxXfinish = false;
                        this.maxYfinish = true;
                    }
                }
                else
                {   //laufe nach unten wenn die aktuelle y-Position kleiner als maxY ist
                    if (this.y <= this.maxY)
                    {   
                        this.y+=this.speed;
                        this.direction = "goDown";
                        
                        //ist y am Ende der Teilstrecke durch einen ungerade speed-groesse goesser als maxY setze y auf maxY
                        //und beende die Teilstrecke
                        if (this.y >= this.maxY)
                        {   
                            this.y = this.maxY;
                            this.maxXfinish = false;
                            this.maxYfinish = true;
                        }
                    } 
                }
            }
        }        
        //Zeichne Gegner mit neuer Position auf dem Canvas
        //x,y, Radius, Farbe
        if (this.dead == false)
        {
             this.draw();   
        }
        else
        {   
            //ist ein Gegner tot wird die letzte Animation ausgeführt
            if(this.lastAni == false && this.spriteFrameID <= 4)
            {
                this.draw();  
            }   
            return;  
        }
    }    

    /*
    Enemy: zeichnet einen Gegner
    Position auf dem Canvas x,y Groesse des Gegners, Farbe
    */
    Enemy.prototype.draw = function(){
                
        var jellycolor = jelly1_walk;
        var spriteDirection = 3;
    
        //aussehen der Gegner
        if(this.dead == false){
            
            switch (this.color){

                case 'yellow':

                    jellycolor = jelly1_walk;
                    break;

                case 'pink':

                    jellycolor = jelly2_walk;
                    break;

                case 'purple':

                    jellycolor = jelly3_walk;
                    break;

                default:
                    break;
            } 
        }
        else{
            switch (this.color){
                
                case 'yellow':

                    jellycolor = jelly1_dead;
                    break;

                case 'pink':

                    jellycolor = jelly2_dead;
                    break;

                case 'purple':

                    jellycolor = jelly3_dead;
                    break;

                default:
                    break;
            }
        }
        
        /*
        SpriteSheet-Aufbau:
        0. Reihe: hoch
        1. Reihe: links
        2. Reihe: runter
        3. Reihe: rechts
        */        
        //In welche Richtung guckt der Gegner
        switch (this.direction){
                
            case "goRight":
                
                spriteDirection = 3;
                break;
            
            case "goLeft":
                
                spriteDirection = 1;
                break;
            
            case "goUp":
                
                spriteDirection = 0;
                break;
                
            case "goDown":
                
                spriteDirection = 2;
                break;
                
            default:
                break;
        }
   
        
        this.findSprite();
        
        var sourceX = (this.spriteFrameID % spriteSheetRows) * spriteSize;
        var sourceY = spriteDirection * spriteSize;
        //Quelle, welcher Bereich auf dem Bild X&Y, wie groß auf dem Bild, wohin auf dem Canvas X&Y, wiegroß auf dem Canvas X&Y
        context.drawImage(jellycolor, sourceX, sourceY, spriteSize, spriteSize, this.x - spriteSize/2, this.y - spriteSize/2, spriteSize, spriteSize);
        
        /*context.save();
        context.beginPath();
        context.moveTo(enemy.x, enemy.y);
        context.fillStyle =  enemy.color;
        context.arc(enemy.x, enemy.y, enemy.radius, 0, 2*Math.PI);
        context.fill();
        context.restore(); 
        context.closePath();*/
    }
    
    /*
    Erscheint ein Gegner auf dem Spielfeld wird sein Start-SpriteFRame zufällig bestimmt. Siehe enemy_draw.
    In dieser Methode wurd alle 0,25 Sekunden der Spriteframe verändert, sodass pro Sekunde
    die Laufanimation einmal komplett durchlaufen wird.
    Hat der Gegner keine Lebenspunkte mehr wird als tot markiert. Siehe tower_shot. Dieser Gegner verschwindet
    erst vom Spielfeld wenn seine letzte Animation abgespielt wurde. Siehe Enemy.prototype.animation.
    Hier wird festgelegt, dass wenn der Gegner tot ist, die SpriteFrameID wieder auf 1 gesetzt wird, damit
    die Animation auch von vorn startet. Wurde die animation einmal komplett durchlaufen wird dies markiert.
    Außerdem wird ein Counter mitgezählt, wieviele letzte Animationen abgespielt wurden. Dies ist wichtig um
    in der draw_canvas erst das Level zu beenden, wenn auch die letzte Animation fertig abgespielt wurde.
    */
    Enemy.prototype.findSprite = function(){
        
        var date = new Date();
        var milliSec = date.getMilliseconds();
        
        //ist der Gegner tot beginne mit der letzte Animation
        if(this.dead == true && this.lastAnibegin == false){
           this.spriteFrameID = 1;
            this.lastAnibegin = true;
        }
        
        /*
        Alle  0,25Sekunden wird die SpriteFrameID erhöht.
        Die beiden SpriteCounter werden benötigt, damit nicht jeden Frame
        die FrameID erhöht wird, sondern nur einmal alle 0,25sek.
        Die beiden Counter wechseln sich mit aktiv sein ab, damit dieser in
        der gleichen Methode wieder zurückgesetzt wird.
        */
        if (milliSec >=0 && milliSec <= 250){
            
            this.spriteCount2 = true;
            
            if(this.spriteCount == true){
                
                //ist eine Animation durchlaufen beginn von vorn außer der Gegner ist tot
                if(this.spriteFrameID == 4 && this.dead == false){
                    this.spriteFrameID = 1;
                    this.spriteCount = false;
                }
                else{
                    this.spriteFrameID++;
                    this.spriteCount = false; 
                    
                    //spiele beim 3. Frame das Bewegungsgeräusch ab
                    if(this.spriteFrameID == 3){
                       sound_walk.play();
                    }
                }
            }
        }
        
        if (milliSec >=251 && milliSec <= 500){
            
           this.spriteCount = true;
            
            if(this.spriteCount2 == true){
                
                if(this.spriteFrameID == 4 && this.dead == false){
                    this.spriteFrameID = 1;
                    this.spriteCount2 = false;
                }
                else{
                    this.spriteFrameID++;
                    this.spriteCount2 = false;  
                    
                    if(this.spriteFrameID == 3){
                       sound_walk.play();
                    }
                }
            }
        }
        
        if (milliSec >=501 && milliSec <= 750){
            
            this.spriteCount2 = true;
            
            if(this.spriteCount == true){
                
                if(this.spriteFrameID == 4 && this.dead == false){
                    this.spriteFrameID = 1;
                    this.spriteCount = false;
                }
                else{
                    this.spriteFrameID++;
                    this.spriteCount = false; 
                    
                    if(this.spriteFrameID == 3){
                       sound_walk.play();
                    }
                }
            }
        }
        
        if (milliSec >=751 && milliSec <= 1000){
            
           this.spriteCount = true;
            
            if(this.spriteCount2 == true){
                
                 if(this.spriteFrameID == 4 && this.dead == false){
                    this.spriteFrameID = 1;
                    this.spriteCount2 = false;
                }
                else{
                    this.spriteFrameID++;
                    this.spriteCount2 = false;
                    
                    if(this.spriteFrameID == 3){
                       sound_walk.play();
                    }
                }
            }
        }
        
        //wurde die letzte Animation komplett durchlaufen
        if(this.spriteFrameID >= 4 && this.dead == true){
            
            //markiere, dass die Animation durchlaufen wurde
            this.lastAni = true;
            //merke dir wieviele Gegner mit der Animation fertig sind
            lastAniCount++;
        }
    }    
    
    //Erzeugt Gegner verschiedener Level    
    function enemy_create(enemy_lvl1, enemy_lvl2, enemy_lvl3){
        
        this.enemy_lvl1 = enemy_lvl1;     //Anzahl der Gegnern mit Level 1
        this.enemy_lvl2 = enemy_lvl2;     //Anzahl der Gegnern mit Level 2
        this.enemy_lvl3 = enemy_lvl3;     //Anzahl der Gegnern mit Level 2
        
        //Enemys Level 1 erstellen
        for (var i = 0; i < (enemy_lvl1); i++)
        {
            enemy_army[i] = new Enemy(
                "enemy_" + i,       //name
                1,                  //Level
                4,                  //Lebenspunkte
                'yellow',           //Aussehen
                0.5,                //Geschwindigkeit kleiner = langsamer
                0.25                //Beute: 2 Gold
            );
         
        }
        
        //Enemys Level 2 erstellen
        for (var i = enemy_lvl1; i < (enemy_lvl1 + enemy_lvl2); i++)
        {
            enemy_army[i] = new Enemy(
                "enemy_" + i,       //name
                2,                  //Level
                6,                  //Lebenspunkte
                'pink',             //Aussehen
                0.75,               //Geschwindigkeit kleiner = langsamer
                0.5                   //Beute: 2 Gold
                    
            );  
        }
        
        //Enemys Level 3 erstellen
        for (var i = enemy_lvl1 + enemy_lvl2; i < (enemy_lvl1 + enemy_lvl2 + enemy_lvl3); i++)
        {
            enemy_army[i] = new Enemy(
                "enemy_" + i,       //name
                3,                  //Level
                13,                 //Lebenspunkte
                'purple',           //Aussehen
                0.7,                //Geschwindigkeit kleiner = langsamer
                1                   //Beute: 4 Gold
                    
            );  
        } 
        
        enemy_counter = enemy_army.length;
        console.log(enemy_counter + " Gegner.");
        
        //array zufällig neusortieren
        /*var shuffle = function(){
            return Math.random()-0.5;
        };
        enemy_army = enemy_army.sort(shuffle);*/
        enemy_army.shuffle();
        
    }    
    
    /*
    Jede Sekunde (spawntime = 60 -> 60fps) wird ein Gegner einem leeren Array hinzugefügt.
    Eine Schleife durchlauft das neue Array und zeichnet alle Gegner die diesem Array
    hinzugefuegt wurden. So erhaelt man eine Spawnwartezeit zwischen den einzelnen Gegnern.
    */
    function enemy_draw(){ 
        var spawntime = 120; //ca. alle 2 Sekunden
        var teilbar = frames % spawntime;
       
        if (teilbar == 1)
        {
            //sind noch nicht alle Gegner in dem neuen Array hinzugefuegt
            if (enemy_army.length != enemy_army_run.length)
            {
                //fuege das naechste Array Elemnt vom Array enemy_army in enemy_army_run ein
                enemy_army_run.push (enemy_army[enemy_num]);
                
                //vergibt zufällige Start SpriteID - damit die Gegner sich unterschiedlich bewegen
                enemy_army[enemy_num].spriteFrameID = Math.floor((Math.random()*(4 - 1 + 1)) +1);
                
                console.log(enemy_army[enemy_num].name + ": Level " + enemy_army[enemy_num].lv + " erschienen.");
                
                enemy_num++; 
                
            } 
            else
            {
                return;
            }
        }
        
        //animiere alle Gegner aus dem Array enemy_army_run
        for(var i = 0; i < enemy_army_run.length; i++)
        {
            enemy_army_run[i].animation(enemy_army_run[i]);
        }
    
    }
    
    //-----------------LEVEL----------------
        
    //Levelklasse
    function Level(name, enemyLv1,enemyLv2,enemyLv3, startgold, waypoint){ 
        
        this.name = name;
        
        this.enemyLv1 = enemyLv1;
        this.enemyLv2 = enemyLv2;
        this.enemyLv3 = enemyLv3;
        
        this.waypoint = waypoint;
        this.startgold = startgold;
        this.isRunning = false;
    }
    
    //Level: Startet das nächste Level.    
    Level.prototype.next = function(){
         
        enemy_num = 0;     
        tower_num = 0;
        lastAniCount = 0;
    
        theCanvas.style.backgroundImage = 'url(images/Background_Lv' + (currentLvl+1) + '.png)';
        //theCanvas.style.backgroundImage = 'url(images/Background_End.png)';
        
        tower.splice(0,tower.length)
        enemy_army.splice(0,enemy_army.length)
        enemy_army_run.splice(0,enemy_army_run.length)
        enemy_create(this.enemyLv1, this.enemyLv2, this.enemyLv3);
        
        gold += this.startgold;
        
        this.isRunning = true;
    }    
    
    //Wegpunkte werden gezeichnet.   
    function path(waypoint){
        this.waypoint = waypoint;        
        context.save();
        context.beginPath();
        context.strokeStyle = 'rgba(170,170,170,0.5)';
        context.lineWidth = 80;
        context.lineJoin = "round";
        context.moveTo(waypoint[0].x, waypoint[0].y);
        
        for(var i=1; i < waypoint.length; i++)        {    
            context.lineTo(waypoint[i].x, waypoint[i].y)
        };        
        context.stroke();
        context.restore();
        context.closePath();
    }      
        
    //---------------SOUNDS-----------------
        
    //Schalte die Musik aus oder an.    
    function musicOff(){
        if(music_on == true){
            music_bg.volume = 0;
            music_lvl.volume = 0;
            bgmusic_slider.value = 0;
            
            music_on = false;
            
            musicOffButton.style.backgroundColor = '#476e9e';
            musicOffButton.style.color = '#b0afaf';
            musicOffButton.innerHTML = "on";
        }
        else{
            
            if(start == false){
                music_bg.play();               
            }
            else{
                music_lvl.play();
            }
            
            music_bg.volume = music_volume;
            music_lvl.volume = music_volume;
            bgmusic_slider.value = music_volume * 100;
            
            music_on = true;
            
            musicOffButton.style.backgroundColor = '#7892c2';
            musicOffButton.style.color = '#ffffff';
            musicOffButton.innerHTML = "off";
        }
    }    
    
    //Regelt die Lautstärke der Musik.    
    function setVolMusic(){
        music_bg.volume = (bgmusic_slider.value) /100;
        music_lvl.volume = (bgmusic_slider.value) /100;
    }    
    
    //Schalte die Geräusche aus oder an.
    function soundOff(){        
        if(sound_on == true){
           
            sound_shot.volume = 0;
            sound_walk.volume = 0;
            sound_dead.volume = 0;
            sound_up.volume = 0;
            sound_damage.volume = 0;
            sounds_slider.value = 0;
            
            sound_on = false;
            
            soundsOffButton.style.backgroundColor = '#476e9e';
            soundsOffButton.style.color = '#b0afaf';
            soundsOffButton.innerHTML = "on";
        }
        else{
            sound_shot.volume = sound_volume;
            sound_walk.volume = sound_volume;
            sound_dead.volume = sound_volume;
            sound_up.volume = sound_volume;
            sound_damage.volume = sound_volume;
            sounds_slider.value = sound_volume * 100;
            
            sound_on = true;
            
            soundsOffButton.style.backgroundColor = '#7892c2';
            soundsOffButton.style.color = '#ffffff';
            soundsOffButton.innerHTML = "off";
        }
    }    
       
    //Regelt die Lautstärke der Sounds.
    function setVolSounds(){
        sound_shot.volume = sounds_slider.value /100;
        sound_walk.volume = sounds_slider.value / 100;
        sound_dead.volume = sounds_slider.value / 100;
        sound_up.volume = sounds_slider.value / 100;
        sound_damage.volume = sounds_slider.value /100;
    } 
}
